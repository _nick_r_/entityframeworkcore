﻿using ProjectStructure.Shared.DTO.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Shared.DTO.Linq
{
    public class FifthTaskDTO
    {
        public string name;
        public IEnumerable<TaskDTO> tasks;

    }
}
