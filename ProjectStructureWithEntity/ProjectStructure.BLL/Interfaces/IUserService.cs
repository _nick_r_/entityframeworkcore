﻿using ProjectStructure.Shared.DTO.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        Task<UserDTO> AddUser(UserDTO user);
        Task<List<UserDTO>> GetAllUsers();
        Task<UserDTO> GetUser(int id);
        Task<UserDTO> UpdateUser(int id, UserDTO user);
        Task DeleteUser(int id);
    }
}
