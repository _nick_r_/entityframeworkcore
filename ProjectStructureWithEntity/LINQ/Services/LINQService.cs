﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LINQ.Models;

namespace LINQ.Services
{
    class LINQService
    {
        private List<Project> projects;
        private List<Models.Task> tasks;
        private List<Team> teams;
        private List<User> users;

        public LINQService()
        {
            projects = new List<Project>(HTTPService.GetProjects().Result);
            tasks = new List<Models.Task>(HTTPService.GetTasks().Result);
            teams = new List<Team>(HTTPService.GetTeams().Result);
            users = new List<User>(HTTPService.GetUsers().Result);
        }

        //Task 1
        public void GetTasks(int authorId)
        {   
            var result = projects.Where(x => x.AuthorId == authorId).Join(tasks, p => p.Id, t => t.ProjectId, (p, t) => new
            {
                projectId = p.Id,
                taskId = t.Id,
                taskName = t.Name,

            }).GroupBy(p => p.projectId).Select(g => new { projectId = g.Key, Count = g.Count() });

            foreach (var i in result)
            {
                Console.WriteLine( "Project with id " + i.projectId + " has " + i.Count + " tasks");
            }        
        }
        //Task 2
        public IEnumerable<Models.Task> GetTasksForPerformer(int performerId)
        {
            var result = tasks.Where(x => (x.PerformerId == performerId) && (x.Name.Length < 45));

            foreach (var task in result)
            {
                Console.WriteLine("Task name: " + task.Name);
            }

            return result;
        }

        //Task 3
        public void GetFinishedTasks(int performerId)
        {
            var finishedTasks = tasks.Where(x => (x.FinishedAt != null) && (x.FinishedAt.Value.Year == 2021) && (x.PerformerId == performerId)).Select(x => new
            {
                id = x.Id,
                name = x.Name
            });           

            Console.WriteLine("Finished tasks in 2021:");
            foreach (var t in finishedTasks)
            {
                Console.WriteLine(t.id + "--" + t.name);
            }
        }

        // Task 4
        public void GetElderUsers()
        {          
            var res = teams.GroupJoin(users,
                t => t.Id,
                u => u.TeamId,
                (team, user) => new
                {
                    teamId = team.Id,
                    teamName = team.Name,
                    Users = user.Where(x => x.BirthDay.Value.Year <= 2012)
                    .OrderByDescending(x => x.RegisteredAt)
                });

            Console.WriteLine("Teams with users:");
            foreach (var team in res)
            {
                Console.WriteLine("Team id: " + team.teamId + " |" + " Team name: " + team.teamName );
                foreach (User us in team.Users)
                {
                    Console.WriteLine("User Name: " + us.FirstName + " | Registered at: " + us.RegisteredAt + " | Birthday: " + us.BirthDay);
                }
                Console.WriteLine("----------------");
            }           
        }

        // Task 5
        public void GetSortedUsersWithTasks()
        {            
            var res = users.OrderByDescending(x => x.FirstName)  
                .GroupJoin(tasks,
                u => u.Id,
                t => t.PerformerId,
                (user, task) => new
                {
                    firstName = user.FirstName,
                    lastName = user.LastName,
                    tasks  = task.OrderByDescending(x => x.Name.Length)
                }
                );

            Console.WriteLine("Users with tasks:");
            foreach (var user in res)
            {
                Console.WriteLine("User name: " + user.firstName + " " + user.lastName);
                foreach (var us in user.tasks)
                {
                    Console.WriteLine("Task name: " + us.Name + " | Task id: " + us.Id);
                }
                Console.WriteLine("----------------");
            }
        }

        // Task 6
        public void GetStructureSix(int userId)
        {
            var structure = users.Where(x => x.Id == userId).GroupJoin(projects, u => u.Id, p => p.AuthorId,
                (u, p) => new
                {
                    user = u,
                    project = p.OrderBy(x => x.CreatedAt).Last(),
                }
                ).GroupJoin(tasks, x => x.user.Id, t => t.PerformerId,
                (u, t) => new
                {
                    user = u.user,
                    project = u.project,
                    amountOfTasksForUser = t.Where(x => x.FinishedAt == null).Count()
                }
                ).GroupJoin(tasks, x => x.project.Id, t => t.ProjectId,
                (u, t) => new
                {
                    user = u.user,
                    lastProject = u.project,
                    amountOfTasksForUser = u.amountOfTasksForUser,
                    amountOfTasksOnLastProject = t.Where(x => x.ProjectId == u.project.Id).Count()
                }
                ).FirstOrDefault();

            Console.WriteLine("User name: " + structure.user.FirstName + " " + structure.user.LastName);
            Console.WriteLine("Last project: " + structure.lastProject.Name);
            Console.WriteLine("Tasks on the last project: " + structure.amountOfTasksOnLastProject);
            Console.WriteLine("Unfinished tasks: " + structure.amountOfTasksForUser);
            

        }

        // Task 7
        public void GetStructureSeven(int projectId)
        {
            var project = projects.Where(x => x.Id == projectId)
                .GroupJoin(tasks, p => p.Id, t => t.ProjectId,
                (p, t) => new
                {
                    project = p,
                    longTask = t.OrderByDescending(x => x.Description.Length).First(),
                    shortTask = t.OrderBy(x => x.Name.Length).First(),
                    quantity = teams.Where(x => x.Id == p.TeamId).GroupJoin(users, t => t.Id, u => u.TeamId,
                    (ts, us) => new
                    {
                        team = ts,
                        quantity = us.Count()
                    }
                    ).Select(x => x.quantity).FirstOrDefault()
                }
                ).FirstOrDefault();

            Console.WriteLine("Project name: " + project.project.Name);
            Console.WriteLine("Longest task: " + project.longTask.Name);
            Console.WriteLine("Shortest: " + project.shortTask.Name);
            Console.WriteLine("Users in team: " + project.quantity);
            
        }

    }
}
