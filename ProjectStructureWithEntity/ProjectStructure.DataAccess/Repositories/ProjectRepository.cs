﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ProjectStructure.DataAccess.Interfaces.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        protected readonly DataContext context;

        public ProjectRepository(DataContext context)
        {
            this.context = context;
        }

        public async Task<Project> Create(Project item)
        {
            await context.Projects.AddAsync(item);
            return item;
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            Project project = await Get(id);
            if (project == null)
                throw new System.Exception("Incorrect id");

            context.Projects.Remove(project);
        }

        public async Task<Project> Get(int id)
        {
            return await context.Projects.FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<IEnumerable<Project>> GetList()
        {
            return await context.Projects.ToListAsync();
        }

        public async Task<Project> Update(int id, Project item)
        {
            Project project  = await Get(id);
            if (project == null)
                throw new System.Exception("Incorrect id");
            project.Deadline = item.Deadline;
            project.Description = item.Description;
            project.Name = item.Name;
            project.Team = item.Team;
            return project;
        }
    }
}
