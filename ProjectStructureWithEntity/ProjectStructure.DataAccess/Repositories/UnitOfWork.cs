﻿using ProjectStructure.DataAccess.Interfaces;
using ProjectStructure.DataAccess.Interfaces.Repositories;
using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private DataContext context;

        private UserRepository userRepository;
        private TeamRepository teamRepository;
        private TaskRepository taskRepository;
        private ProjectRepository projectRepository;

        public UnitOfWork(DataContext context)
        {
            this.context = context;
        }

        public IRepository<User> Users
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(context);
                return userRepository;
            }
        }

        public IRepository<Team> Teams
        {
            get
            {
                if (teamRepository == null)
                    teamRepository = new TeamRepository(context);
                return teamRepository;
            }
        }

        public IRepository<Models.Task> Tasks
        {
            get
            {
                if (taskRepository == null)
                    taskRepository = new TaskRepository(context);
                return taskRepository;
            }
        }

        public IRepository<Project> Projects
        {
            get
            {
                if (projectRepository == null)
                    projectRepository = new ProjectRepository(context);
                return projectRepository;
            }
        }

        public void SaveChages()
        {
            context.SaveChanges();
        }

        public async System.Threading.Tasks.Task SaveChangesAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}
