﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess.Models.Enums
{
    public enum State
    {
        Started,
        InProgress,
        Testing,
        Finished
    }
}
