﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            if (Database.EnsureCreated())
                Seed();
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Models.Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }


        public void Seed()
        {
            Team team1 = new Team
            {
                Name = "Best team ever",
                CreatedAt = new DateTime(2017, 3, 17),
            };

            Team team2 = new Team
            {
                Name = "Terrible team",
                CreatedAt = new DateTime(2021, 3, 17),
            };

            this.Teams.AddRange(team1, team2);

            User user = new User
            {
                FirstName = "Elon",
                LastName = "Musk",
                RegisteredAt = new DateTime(2012, 3, 17),
            };

            User user1 = new User
            {
                FirstName = "Dean",
                LastName = "Winchester",
                RegisteredAt = new DateTime(2017, 3, 17),
                Team = team1
            };

            User user2 = new User
            {
                FirstName = "Sam",
                LastName = "Winchester",
                RegisteredAt = new DateTime(2016, 1, 20),
                Team = team1
            };

            User user3 = new User
            {
                FirstName = "John",
                LastName = "Smith",
                RegisteredAt = new DateTime(2020, 1, 17),
                Team = team2               
            };

            this.Users.AddRange(user, user1, user2, user3);

            Project project1 = new Project
            {
                Author = user,
                Team = team1,
                Name = "Global memes supply",
                Description = "Everyone likes memes",
                CreatedAt = new DateTime(2020, 1, 16),
            };

            this.Projects.Add(project1);

            Models.Task task1 = new Models.Task
            {
                Project = project1,
                Performer = user1,
                Name = "Interesting task",
                Description = "Everyone would like to do it",
                State = Models.Enums.State.Finished,
                CreatedAt = DateTime.Now,
                FinishedAt = new DateTime(2020, 2, 20),
            };

            Models.Task task2 = new Models.Task
            {
                Project = project1,
                Performer = user1,
                Name = "Second task",
                Description = "Easy one",
                State = Models.Enums.State.Started,
                CreatedAt = DateTime.Now
            };

            Models.Task task3 = new Models.Task
            {
                Project = project1,
                Performer = user2,
                Name = "Third task",
                Description = "Easy one",
                State = Models.Enums.State.Started,
                CreatedAt = DateTime.Now
            };

            this.Tasks.AddRange(task1, task2, task3);

            this.SaveChanges();

        }
    }
 }
